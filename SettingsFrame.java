import controlP5.*;
import processing.core.*;

import processing.serial.Serial;

import java.util.*;

class SettingsFrame extends PApplet {
    //Размеры.
    static final int width_ = 250, height_ = 250;  
  
    //GUI.
    ControlP5 controlP5_;
    
    //Запустить основное окно.
    IRocketGUICallBacks runCallBack_;
    
    
    Textlabel serialTitleLabel_;
    ScrollableList serialsListBox_;
    
    //Порт, который мы выбрали.
    String choosedSerial_;
    
    
    public SettingsFrame(IRocketGUICallBacks runCallBack) {
        super();
        
        runCallBack_ = runCallBack;
        
        PApplet.runSketch(new String[]{this.getClass().getName()}, this);
    }
  
    public void settings() {
        size(width_, height_);
    }
  
    public void setup() {
        //Название окна.
        surface.setTitle("Settings");
        
        //Устанавливаем по середине.
        surface.setLocation((displayWidth - width) / 2, (displayHeight - height) / 2);
        
        
        controlP5_ = new ControlP5(this);
        
        //Заголовок.
        serialTitleLabel_ = controlP5_.addTextlabel("SerialTitleLabel")
            .setText("Choose serial port:")
            .setPosition(5, 5);
        
        //Выбор serial.
        serialsListBox_ = controlP5_.addScrollableList("SerialsListBox")
                                                  .setPosition(5, 20)
                                                  .setSize(width_ - 10, height_)
                                                  .setBarHeight(20);
        
        //Начальное значение.
        serialsListBox_.getCaptionLabel().set("No...");
        
        //Кнопка запустить.
        controlP5_.addButton("Run")
           .setPosition(width_ - 90, height_ - 50)
           .setSize(70,30);
        
    }
  
    public void draw() {
        background(100);
        
        serialTitleLabel_.draw(this);
        
        //Обновляет список.
        serialsListBox_.setItems(Serial.list());
        
        //Грабим фокус.
        surface.setVisible(true);
    }
    
    //Вызывается кнопкой Run.
    public void Run(){
        if(choosedSerial_ == null || !Arrays.asList(Serial.list()).contains(choosedSerial_)){
            controlP5_.get(ScrollableList.class, "SerialsListBox").open();
            
            return;
        }
        
        //Ресурсы.
        surface.setVisible(false);
        noLoop();
        
        runCallBack_.RunGUICallBack(1280, 720, choosedSerial_);
    }
    
    //Когда выбирается один из параметров.
    public void SerialsListBox(int n){
        choosedSerial_ = serialsListBox_.getItem(n).get("text").toString();
    }
}
