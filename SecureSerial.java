import processing.serial.Serial;

import java.util.Arrays;

public class SecureSerial{
    //Поток данных.
    private Serial internalSerial_;  
    
    //Текущее состояние.
    private byte recvDataGrammState_ = 0;
    
    //Буфер приёма.
    private byte[] recvBuff_;
    //Смещение.
    private short recvBuffOffset_;
    
    //Длина датаграммы.    
    private short recvDataGrammLength_;
    //Хеш датаграммы.
    private short recvDataGrammHash_;
    //Тип датаграммы.
    private byte recvDataGrammType_;
    
    //Обработчик датаграмм.
    private ISecureSerialCallBacks receivedDataGrammHandler_;
    
    //Конструктор.
    public SecureSerial(Serial internalSerial, ISecureSerialCallBacks receivedDataGrammHandler){
        internalSerial_ = internalSerial;
        
        receivedDataGrammHandler_ = receivedDataGrammHandler; 
        
        recvDataGrammState_ = 0;
        recvBuff_ = new byte[128];
        
        //serialEvent будет вызываться когда есть 8 байт, т.к. минимальная длина пакета 8.
        internalSerial_.buffer(8);
    }
    
    public void SerialEventUpdate(){
        while(internalSerial_.available() > 0){
            short recvByte = (short)internalSerial_.read();
            
            //Преамбула.
            if(recvDataGrammState_ == 0 && recvByte == 0xFF){
                recvDataGrammState_++; continue;
            }
            if(recvDataGrammState_ == 1 && recvByte == 0x00){
                recvDataGrammState_++; continue;
            }
            if(recvDataGrammState_ == 2 && recvByte == 0xAA){
                recvDataGrammState_++; continue;
            }
            if(recvDataGrammState_ == 3 && recvByte == 0x55){
                recvDataGrammState_++; continue;
            }
            
            //Тип.
            if(recvDataGrammState_ == 4){
                recvDataGrammType_ = (byte)recvByte;
                recvDataGrammState_++; continue;
            }
            
            //Длина.
            if(recvDataGrammState_ == 5 && recvByte > 0 && recvByte <= recvBuff_.length){
                recvDataGrammLength_ = recvByte;
                recvBuffOffset_      = 0;
                    
                recvDataGrammState_++; continue;
            }
            
            //Данные.
            if(recvDataGrammState_ == 6){
                recvBuff_[recvBuffOffset_++] = (byte)recvByte;
                
                if(recvBuffOffset_ == recvDataGrammLength_){
                    recvDataGrammState_++;
                }
                
                continue;
            }
            
            //Хеш.
            if(recvDataGrammState_ == 7){
                recvDataGrammHash_ = recvByte;
                
                recvDataGrammState_++; continue;
            }
            if(recvDataGrammState_ == 8){
                recvDataGrammHash_ |= recvByte << 8;
                
                receivedDataGrammHandler_.DataGrammReceivedCallBack(recvBuff_, recvDataGrammLength_, recvDataGrammType_, false);
            }
            
            recvDataGrammState_ = 0;
        }
    }
    
    public void SendDataGramm(byte[] buff, short len, byte type){
        
        //Преамбула.
        internalSerial_.write(0xFF);
        internalSerial_.write(0x00);
        internalSerial_.write(0xAA);
        internalSerial_.write(0x55);
        
        //Заголовок.
        internalSerial_.write(type);
        internalSerial_.write((byte)len);
        
        //Данные.
        internalSerial_.write(Arrays.copyOfRange(buff, 0, len));
        
        //Хеш.
        internalSerial_.write(1);
        internalSerial_.write(1);
    }
}
