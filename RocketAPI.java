import processing.serial.Serial;

import processing.core.*;

import toxi.geom.Quaternion;

import processing.core.*;


import java.nio.ByteOrder;
import java.nio.ByteBuffer;

import java.util.Formatter;

class RocketAPI implements ISecureSerialCallBacks{
    //Поток данных с ракеты.
    private SecureSerial rocketSerial_;
     
    //Ориентация ракеты.
    private Quaternion orientation_;
    
    //Для доступа к gui.
    private IRocketAPICallBacks callBacks_;
    
    public RocketAPI(Serial rocketSerial, IRocketAPICallBacks callBacks){      
        //Создаём защищённое соединение.  
        rocketSerial_ = new SecureSerial(rocketSerial, this);
        
        //Управление gui.
        callBacks_ = callBacks;
        
        orientation_ = new Quaternion(1, 0, 0, 0);
    }
    
    public void SerialEventUpdate(){
        rocketSerial_.SerialEventUpdate();
    }
    
    public float[] getOrientation(){
        return orientation_.toAxisAngle();
    }
    
    public void Send(){
        byte[] buff = "Hello".getBytes();
      
        rocketSerial_.SendDataGramm(buff, (short)buff.length, (byte)13);
    }
    
    //Вызывается, когда что-то приходит.
    public void DataGrammReceivedCallBack(byte[] buff, short len, byte type, boolean isVerify){
        switch(type){
            //TYPE_TEXT
            case 0:
                callBacks_.RocketConsoleUpdateCallBack(new String(buff, 0, len));
                break;
                
            //TYPE_RESPONCE_SENSORS_DATA
            case 2:
                if(!isVerify){
                    //TODO Вывод в консоль об ошибке.
                    //return;
                }
                
                ByteBuffer rawData =  ByteBuffer.wrap(buff, 0, len);

                rawData.order(ByteOrder.LITTLE_ENDIAN);
                
                orientation_.set(rawData.getFloat(), rawData.getFloat(), rawData.getFloat(), rawData.getFloat());
                
                //Formatter f = new Formatter();
                
                //f.format("%.2f %.2f %.2f %.2f", rawData.getFloat(), rawData.getFloat(), rawData.getFloat(), rawData.getFloat());
                
                //callBacks_.RocketConsoleUpdateCallBack(Float.toString(rawData.getFloat()) + " " + Float.toString(rawData.getFloat()) + " " + Float.toString(rawData.getFloat()) + " "  + Float.toString(rawData.getFloat()));
                
                //callBacks_.RocketConsoleUpdateCallBack("Recv");
                
                break;
        }
    }
}
