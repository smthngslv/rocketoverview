import processing.serial.Serial;
import toxi.geom.Quaternion;

//GUI
import controlP5.*;


//Backend.

//Доступ к системе ракеты.
RocketAPI rocketAPI_;

//Frontend.

//Наш gui менеджер.
ControlP5 controlP5_;

//Консоль
Println rocketConsole_;

//Загрузочный экран.
PImage splashScreenImg_;

//3D модель ракеты.
PShape rocketModel_;


void settings(){
    splashScreenImg_ = loadImage("data/splashScreen.png");
    
    //Размер берем у картинки, 3D рендер.
    size(splashScreenImg_.width, splashScreenImg_.height, P3D);
    
    //Отключаем обновление экрана.
    noLoop();
}


void setup(){
    //Отображаем splash.
    image(splashScreenImg_, 0, 0);
    
    //Устанавливаем окно по середине экрана.
    surface.setLocation((displayWidth - width) / 2, (displayHeight - height) / 2);
    
    //Открываем окно настроек.
    new SettingsFrame(
        new IRocketGUICallBacks(){
            public void RunGUICallBack(int w, int h, String serial){
                surface.setSize(w, h);
              
                BuildLayoutAndInitApi(serial);
                
                loop();
            }
        }
    );
    
    
    //В отдельном потоке загружаем ресурсы.
    thread("InitializeThread");
}

//Вызывается в отдельном потоке.
void InitializeThread(){
    //Тут можно выполнять долгие операции.
    rocketModel_ = loadShape("data/rocketModel.obj");
    
    rocketModel_.rotateX(PI/2.0f);
    rocketModel_.scale(0.5);
    rocketModel_.setFill(0xfff0f0f0);
}


void BuildLayoutAndInitApi(String serial){
    controlP5_ = new ControlP5(this);
    
    Textarea myTextarea = controlP5_.addTextarea("RocketConsole")
        .setPosition(0, 0)
        .setSize(250, height)
        .setFont(createFont("Arial", 10))
        .setLineHeight(14)
        .hideScrollbar()
        //.setColor(color(200))
        //.setColorBackground(color(0, 100))
        //.setColorForeground(color(255, 100))
        ;
    ;
  
    rocketConsole_ = controlP5_.addConsole(myTextarea);//
    
    rocketAPI_ = new RocketAPI(new Serial(this, serial, 115200), new IRocketAPICallBacks(){
            public void RocketConsoleUpdateCallBack(String upd){              
                print(upd); //<>//
            }
        }
    );
}


//Вызывается автоматически, когда есть данные в порту.
void serialEvent(Serial port){
    rocketAPI_.SerialEventUpdate();
}


void draw(){
    if(rocketAPI_ == null){
        return;
    }
  
    
    background(50);
    
    
    
    pushMatrix();
    
    pointLight(255, 255, 255, mouseX, mouseY, 1000);
    
    //Рисуем в центре.
    translate(width / 2, height / 2);
    
    //Получаем углы.
    float[] axis = rocketAPI_.getOrientation();
    
    //Поворачиваем.
    rotate(axis[0], -axis[1], axis[3], axis[2]);
    
    //Отображаем модель.
    shape(rocketModel_);
    
    popMatrix();

}

void mousePressed() {
    if(mouseButton == LEFT){
        rocketAPI_.Send();
    }
}
